import json
import sys
from datetime import datetime

data = {}
data["queries"] = []

for line in sys.stdin:
    qid, qhash, query, qdt = line.strip().split("\t")
    
    
    data["queries"].append({
        "creation_time": datetime.strptime(qdt, "%Y-%m-%d %H:%M:%S.%f").strftime("%a, %d %b %Y %H:%M:%S -0000"),
        "qid": qid,
        "qstr": query
    })


print json.dumps(data, sort_keys=True,
                  indent=4, separators=(',', ': '))